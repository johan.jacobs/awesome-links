# Collection of Awesome Links


## GStreamer

* [Using GStreamer](https://www.youtube.com/watch?v=ZphadMGufY8)
* [Documentation](https://gstreamer.freedesktop.org/documentation//?gi-language=c)
* [C API reference](https://gstreamer.freedesktop.org/documentation//libs.html?gi-language=c)
* What is GStreamer
    * An open source pipeline-based cross-platform extensible multimedia frmework.


## RTP / RTSP
* Real time Transport Protocol
    * Delivering audio or video across an IP network
* Real Time Streaming Protocol
    * Controlling the RTP
    * Comparable to HTTP but has state and other types of messages
    * Port 554


## Git

* [Dan Gitschooldude](https://www.youtube.com/channel/UCshmCws1MijkZLMkPmOmzbQ/)
* [Comparing Workflows](https://www.atlassian.com/git/tutorials/comparing-workflows)
* [Git fast forwards and branch management](https://confluence.atlassian.com/bitbucket/git-fast-forwards-and-branch-management-329977726.html)
* [Git Branching - Rebasing](https://git-scm.com/book/en/v2/Git-Branching-Rebasing#The-Perils-of-Rebasing)
* [Git team workflows: merge or rebase?](https://www.atlassian.com/git/articles/git-team-workflows-merge-or-rebase)


## CI/CD

* [Gitlab CI/CD Deep Dive Demo](https://about.gitlab.com/handbook/marketing/product-marketing/demo/cicd-deep/)
* [GitLab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ee/ci/yaml/)


## C Programming

* [C Keywords](http://tigcc.ticalc.org/doc/keywords.html)
* [const and static keywords](http://www.xs-labs.com/en/blog/2011/07/29/c-const-static-keywords/)
* [Low Level Operators and Bit Fields](http://www.cs.cf.ac.uk/Dave/C/node13.html)
* [Little and Big Endian Mystery](https://www.geeksforgeeks.org/little-and-big-endian-mystery/)
* [Seventeen steps to safer C code](https://www.embedded.com/design/programming-languages-and-tools/4215552/Seventeen-steps-to-safer-C-code)
* [Does GCC's \_\_attribute__((\_\_packed__)) retain the original ordering?](http://stackoverflow.com/questions/1756811/does-gccs-attribute-packed)
* [Is gcc's \_\_attribute__((\_\_packed__)) / #pragma pack unsafe?](http://stackoverflow.com/questions/8568432/is-gccs-attribute-packed-pragma-pack-unsafe)
* [Reentrancy](http://www.ganssle.com/articles/areentra.htm)
* [Table-driven State Machine using function pointers in C](https://kjarvel.wordpress.com/2011/10/26/table-driven-state-machine-using-function-pointers-in-c/)
* [Implementing Efficient State Machines](http://www.conman.org/projects/essays/states.html)
* TODO:
  * Definition and declaration
  * Struct forward declaration


## Embedded Development (microcontrollers, cpu, rtos, ...)

* [MCU Programming: Basics](https://www.renesas.com/en-eu/support/technical-resources/engineer-school/mcu-programming-peripherals-01-gpio.html)
* [Introduction to Microcontrollers - Beginnings](https://www.embeddedrelated.com/showarticle/453.php)
* [8 pillars of embedded software](https://www.edn.com/electronics-blogs/embedded-basics/4442167/8-pillars-of-embedded-software)
* [RTOS or bare metal - five decisive factors](https://www.edn.com/electronics-blogs/embedded-basics/4441028/RTOS-or-bare-metal---five-decisive-factors)
* [Mastering stack and heap for system reliability: Part 1 – Calculating stack size](https://www.embedded.com/design/debug-and-optimization/4394132/Mastering-stack-and-heap-for-system-reliability--Part-1---Calculating-stack-size)
* [Interrupts short and simple (1)](https://www.embedded.com/design/programming-languages-and-tools/4397803/Interrupts-short-and-simple--Part-1---Good-programming-practices)
* [Interrupts short and simple (2)](https://www.embedded.com/design/programming-languages-and-tools/4398340/Interrupts-short---simple--Variables--buffers---latencies)
* [Interrupts short and simple (3)](https://www.embedded.com/design/programming-languages-and-tools/4398710/Interrupts-short-and-simple--Part-3---More-interrupt-handling-tips)
* [Embedded device driver design: Interrupt handling](https://www.embedded.com/design/programming-languages-and-tools/4420109/Embedded-device-driver-design--Interrupt-handling)
* [Using pointer arrays: Part 1 - A timer driver](https://www.embedded.com/design/programming-languages-and-tools/4418931/Using-pointer-arrays--Part-1---A-timer-driver)
* [An introduction to function pointers: Part 1](https://www.embedded.com/design/programming-languages-and-tools/4418929/An-introduction-to-function-pointers--Part-1)
* [RTOS Revealed](https://www.embedded.com/collections/4443063/RTOS-Revealed)
* [Multitasking on an AVR](https://xivilization.net/~marek/binaries/multitasking.pdf)
* [Mutexes and Semaphores Demystified](https://barrgroup.com/Embedded-Systems/How-To/RTOS-Mutex-Semaphore)
* [How to Use C's volatile Keyword](http://www.barrgroup.com/Embedded-Systems/How-To/C-Volatile-Keyword)
* [Volatile: The Multithreaded Programmer's Best Friend](http://www.drdobbs.com/cpp/volatile-the-multithreaded-programmers-b/184403766)
* [Free online course on Embedded Systems](http://www.eeherald.com/section/design-guide/esmod.html)
* [100% Open-Source Development for the Cortex-M by Mike Anderson](https://www.youtube.com/watch?v=FEPGxU8u0oc)
* TODO:
  * Priority inversion
  * Deadlocks
  * Race conditions
  * Preemptive and cooperative scheduling


## Linux Internals

* [Tutorial: Building the Simplest Possible Linux System - Rob Landley](https://www.youtube.com/watch?v=Sk9TatW9ino)
* [A book-in-progress about the linux kernel and its insides](https://0xax.gitbooks.io/linux-insides/)
* [What Stable Kernel Should I Use?](http://kroah.com/log/blog/2018/08/24/what-stable-kernel-should-i-use/)
* [Introduction to Memory Management in Linux](https://www.youtube.com/watch?v=7aONIVSXiJ8)
* [Introduction to Memory Management in Linux by Alan Ott](https://www.youtube.com/watch?v=EWwfMM2AW9g)
* [The TTY demystified](http://www.linusakesson.net/programming/tty/)
* [A Brief Introduction to termios (1)](https://blog.nelhage.com/2009/12/a-brief-introduction-to-termios/)
* [A Brief Introduction to termios (2)](https://blog.nelhage.com/2009/12/a-brief-introduction-to-termios-termios3-and-stty/)
* [Using pseudo-terminals (pty) to control interactive programs](http://rachid.koucha.free.fr/tech_corner/pty_pdip.html)


## Debian

* [Debian for Embedded Systems: Best Practices](https://www.youtube.com/watch?v=vfo25WwklQ8)
* [Debos](https://github.com/go-debos/debos)


## Embedded Linux

* [Debian ArmHardFloatPort](https://wiki.debian.org/ArmHardFloatPort)
* [Custom Embedded Linux Distributions](http://www.linuxjournal.com/content/custom-embedded-linux-distributions)
* [Cross-Bootstrap Fedora](https://dvdhrm.github.io/2018/01/09/cross-bootstrap-fedora/)
* [The end of embedded Linux (as we know it)](https://elinux.org/images/c/c2/Csimmonds-end-of-embedded-linux.pdf)
* [Linux Board Porting Series - Module 3 - Linux Boot Process](https://training.ti.com/linux-board-porting-series-module-3-linux-boot-process)
* [Anatomy of Cross-Compilation Toolchains](https://www.youtube.com/watch?v=Pbt330zuNPc)
* [Embedded Linux Size Reduction Techniques](https://www.youtube.com/watch?v=ynNLlzOElOU)
* [Demystifying Systemd for Embedded Systems](https://www.youtube.com/watch?v=ERS1OSOTGpg)
* [Understanding a Real-Time System](https://www.youtube.com/watch?v=wAX3jOHHhn0)
* [Introduction to Realtime Linux](https://www.youtube.com/watch?v=BKkX9WASfpI)

### Yocto
* [Reference Manual](https://www.yoctoproject.org/docs/current/ref-manual/ref-manual.html)
* [Mega Manual](https://www.yoctoproject.org/docs/current/mega-manual/mega-manual.html)
* [Building BeagleBone Systems with Yocto](https://jumpnowtek.com/beaglebone/BeagleBone-Systems-with-Yocto.html)


## Linux System Programming

* [Socket Programming in C](https://www.geeksforgeeks.org/socket-programming-cc/)
* [Sockets Tutorial](http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html)
* [Asynchronous I/O and event notification on linux](http://davmac.org/davpage/linux/async-io.html)
* [Non-Blocking accept()/connect()](http://www.sourcexr.com/articles/2014/02/01/non-blocking-accept-connect)
* [Nonblocking I/O](https://medium.com/@copyconstruct/nonblocking-i-o-99948ad7c957)
* [Understanding Reactor Pattern: Thread-Based and Event-Driven](https://dzone.com/articles/understanding-reactor-pattern-thread-based-and-eve)
* [Soft Real-Time Programming with Linux](http://www.drdobbs.com/soft-real-time-programming-with-linux/184402031)
* [Over and over again: periodic tasks in Linux](http://www.2net.co.uk/tutorial/periodic_threads)
* [Using I2C from userspace in Linux](https://xgoat.com/wp/2007/11/11/using-i2c-from-userspace-in-linux/)
* [Interfacing with I2C Devices](https://elinux.org/Interfacing_with_I2C_Devices)
* [IPC: Shared Memory](http://users.cs.cf.ac.uk/Dave.Marshall/C/node27.html)
* [Serial Programming Guide for POSIX Operating Systems](http://www.cmrr.umn.edu/~strupp/serial.html)
* [Serial Programming HOWTO](http://tldp.org/HOWTO/Serial-Programming-HOWTO/index.html)
* [Shared Libraries: Understanding Dynamic Loading](https://amir.rachum.com/blog/2016/09/17/shared-libraries/)
* [Library order in static linking](https://eli.thegreenplace.net/2013/07/09/library-order-in-static-linking)
* [Better understanding Linux secondary dependencies solving with examples](http://www.kaizou.org/2015/01/linux-libraries/)


## Software Architecture & Concepts

* [Robert C Martin - Clean Architecture and Design](https://www.youtube.com/watch?v=Nsjsiz2A9mg)
* [Rob Pike - 'Concurrency Is Not Parallelism'](https://www.youtube.com/watch?v=cN_DpYBzKso)
* [Understanding a Real-Time System by Steven Rostedt](https://www.youtube.com/watch?v=wAX3jOHHhn0)


## Linux IoT

* [Modern deployment for Embedded Linux and IoT](https://media.ccc.de/v/ASG2017-112-modern_deployment_for_embedded_linux_and_iot)
* [WolfSSL Manual - Chapter 11:  SSL Tutorial](https://www.wolfssl.com/docs/wolfssl-manual/ch11/)
* [An Intro to IoT Protocols: MQTT, CoAP, HTTP & WebSockets](https://www.youtube.com/watch?v=s6ZtfLmvQMU)
* [Comparing Messaging Techniques for the IoT - Michael E Anderson, The PTR Group, inc.](https://www.youtube.com/watch?v=6DRFAOTGdjA)
* [Mighty Messaging Patterns](https://www.youtube.com/watch?v=rWz8OoVuDls)
* [A Quick Guide to Understanding IoT Application Messaging Protocols](https://www.einfochips.com/blog/a-quick-guide-to-understanding-iot-application-messaging-protocols/)
* [MQTT Essentials](https://www.hivemq.com/mqtt-essentials/)
* [NATS: A Central Nervous System for IoT Messaging](https://nats.io/blog/openiot/)


## Embedded Hardware

* [LeMaker](http://www.lemaker.org)
* [Khadas](https://www.khadas.com)
* [Router board (x86 based)](https://www.pcengines.ch/apu2.htm)


## IO Hardware

* [GSM Modems](https://www.siretta.co.uk/2g-3g-4g-modems-f-104.php)


## DIY Project

* [Zerophone](https://www.crowdsupply.com/arsenijs/zerophone)


## Raspberry Pi

* [How The Raspberry Pi Boots Up](https://thekandyancode.wordpress.com/2013/09/21/how-the-raspberry-pi-boots-up/)
* [Understanding the Raspberry Pi Boot Process](http://wiki.beyondlogic.org/index.php?title=Understanding_RaspberryPi_Boot_Process)
* [How does Raspberry Pi's boot loader work?](https://stackoverflow.com/questions/16317623/how-does-raspberry-pis-boot-loader-work)

## LXC

* [Hypervisor](https://en.wikipedia.org/wiki/Hypervisor)
* [Debian Virtualization: LXC Application Containers](https://l3net.wordpress.com/2013/08/10/debian-virtualization-lxc-application-containers/)


## Nginx

* [Understanding Nginx HTTP Proxying, Load Balancing, Buffering, and Caching](https://www.digitalocean.com/community/tutorials/understanding-nginx-http-proxying-load-balancing-buffering-and-caching)
* [Use NGINX as a Front-end Proxy and Software Load Balancer](https://linode.com/docs/uptime/loadbalancing/use-nginx-as-a-front-end-proxy-and-software-load-balancer/)
* [Introduction to modern network load balancing and proxying](https://blog.envoyproxy.io/introduction-to-modern-network-load-balancing-and-proxying-a57f6ff80236)
* [How To Configure Nginx with SSL as a Reverse Proxy for Jenkins](https://www.digitalocean.com/community/tutorials/how-to-configure-nginx-with-ssl-as-a-reverse-proxy-for-jenkins)
* [OpenSSL Essentials: Working with SSL Certificates, Private Keys and CSRs](https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs)
* [Creating a Self-Signed SSL Certificate](https://devcenter.heroku.com/articles/ssl-certificate-self)


## Golang

* [Why Go Is Successful (aka Simplicity is Complicated)](https://www.youtube.com/watch?v=k9Zbuuo51go)
* [Building a Bank with Go](https://www.youtube.com/watch?v=y2j_TB3NsRc)


## Cloud

* [CNCF](https://www.cncf.io/)
* [Building Better Containers: A Survey of Container Build Tools](https://www.youtube.com/watch?v=5D_SqLv92V8)
* [Monitoring, the Prometheus Way](https://www.youtube.com/watch?v=PDxcEzu62jk)
* [End-to-end monitoring with the Prometheus Operator](https://www.youtube.com/watch?v=-cw_LS4C2Ew)
* [Deploy code faster: with CI/CD and Kubernetes](https://cloud.google.com/kubernetes-engine/kubernetes-comic/)
* [Kubernetes Playground](https://www.katacoda.com/courses/kubernetes/playground)
* [Project Atomic](https://www.projectatomic.io/)
* [RancherOS](https://rancher.com/rancher-os/)
* [Caddy - The HTTP/2 Web Server with Automatic HTTPS](https://caddyserver.com/)
* [An overview of Project Atomic](https://lwn.net/Articles/747576/)
* [Containers without Docker at Red Hat](https://lwn.net/Articles/741841/)
* [DigitalRebar Provision deploy Docker's LinuxKit Kubernetes](https://www.youtube.com/watch?v=kITojfeYaPQ)
* [Harbor](https://goharbor.io)


## TED talks

* [How to get better at the things you care about](https://www.ted.com/talks/eduardo_briceno_how_to_get_better_at_the_things_you_care_about)
* [Are you a giver or a taker?](https://www.ted.com/talks/adam_grant_are_you_a_giver_or_a_taker)
* [How to speak up for yourself](https://www.ted.com/talks/adam_galinsky_how_to_speak_up_for_yourself)
* [Your body language may shape who you are](https://www.ted.com/talks/amy_cuddy_your_body_language_shapes_who_you_are)
* [How to speak so that people want to listen](https://www.ted.com/talks/julian_treasure_how_to_speak_so_that_people_want_to_listen)
* [Inside the mind of a master procrastinator](https://www.ted.com/talks/tim_urban_inside_the_mind_of_a_master_procrastinator)
* [The surprising habits of original thinkers](https://www.ted.com/talks/adam_grant_the_surprising_habits_of_original_thinkers)
* [Where good ideas come from](https://www.ted.com/talks/steven_johnson_where_good_ideas_come_from)

