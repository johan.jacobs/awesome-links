# Cpp tips post course

* don't use C, use C++ whenever possible
* C++ exception handling preferred
* use lambda function as callbacks instead of function pointers
    * lambda function are function objects
    * better be better optimized by compiler (can be made inline)
    * mutable keyword
* C++17 filesystem library (experimental/filesystem)
* prefer c++ aliasing over C typedef
* prefer constexpr
    * calculated on compile time
    * don't use macros
        * not typesafe
* don't do manual memory management
    * sensitive for leaks
    * use smart pointers instead
        * code is cleaner and more understandable
* keep range based for loops in mind
    * for (const auto &x : vec)
* Override and final specifiers
* Resource acquisition is initialization idiom RAII
* Concurrency
    * Instead of mutex use lock guard
        * Try to prevent locking in general, better to give every thread its
          own variable and combine them afterwards (mapreduce)
* Standard Library
    * Sequence containers
        * std::array
            * Random access
            * Static size
        * std::vector
            * Random access
            * Grows dynamically
        * std::deque
            * Like vector but can grow at both ends
            * Commenly used to implement queue's
        * std::list
            * two way linked list
            * Fast insert/erase
            * No random access
        * std::forward_list
            * linked list
    * Associative Containers
        * std::set
            * Binary tree
            * Sorted
            * Fast search
            * No random access
            * Sort orders in functional header
        * std::multiset
            * Allows duplicates
        * std::map
            * Binary tree
            * key - value
            * sorted based on key
            * uses pair structures
        * std::multimap
            * can store multiple values for same key
    * Unordered containers
        * Internally implemented as hash tables
        * Fast search
        * Fast insertion deletion
        * std::unordered_set
        * std::unodered_map
            * stores pair opjects
        * std::unordered_multimap
            * allows duplicates
        * std::hash
            * needed to store user defined types in unordered_containers
            * need function object to hash custom object
